package virta.jet.embedded.debug;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import virta.jet.embedded.servlet.JetServlet;
import virta.sys.Shell;

@SuppressWarnings("unused")
public class RootServlet extends JetServlet {

	private static final long serialVersionUID = 68066920756599663L;

	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws IOException, ServletException{
		
		//VARs
		
		//PAGE (this is very bad style to generate HTML from Java as for me. This one is just example for Servlet contains)
		StringBuffer sb = new StringBuffer();
		
		sb.append("<h1>Jet Server</h1>");
		sb.append("<h2>Visit here for demo</h2>");
		sb.append("<ul>");
		sb.append("<li><a href=\"/debug\">DebugServlet</a> with some debug info</li>");
		sb.append("<li><a href=\"/lang\">LangServlet</a> with i18n example (use ?hl=ru (for Russian))</li>");
		sb.append("</ul>");
		//Reply
		resp.addHeader("Powered-By","Jet");
		
		super.sendRequest(req);
		super.sendResponse(resp);
		super.useDefaults();
		
		super.reply(sb.toString());
		
	}
}
