package virta.jet.embedded.debug;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import virta.jet.embedded.servlet.JetServlet;

public class LangServlet extends JetServlet {

	private static final long serialVersionUID = -1816494299996572558L;
	
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws IOException, ServletException{
		
		
		String hl = req.getParameter("hl");
		
		//init
		String hello=null;
		String jetty=null;
		
		if(hl!=null){
			if(hl.equals("ru")){
				hello = "Привет";
				jetty = "Этот сайт работает на сервере Jetty c оберткой Jet от VirtaLab";
				
			} else if(hl.equals("fi")) {
				hello = "Terve";
				jetty = "Tämä sitesto tyot Jetty Web Server:ssa Jet kääre alkaen VirtaLab";
			} else {
				//default case
				hello = "Hello";
				jetty = "This site is powered by Jetty Web Server with Jet wrapper by VirtaLab"; 
			}	
		} else {
			hello = "Hello";
			jetty = "This site is powered by Jetty Web Server with Jet wrapper by VirtaLab";
		}
		
		StringBuffer body = new StringBuffer();
		body.append("<h1>");
		body.append(hello);
		body.append("</h1>");
		body.append(jetty);
		
		//reply
		super.sendRequest(req);
		super.sendResponse(resp);
		super.useDefaults();
		super.reply(body.toString());
		
	}
}
