package virta.jet.embedded.debug;

import virta.jet.embedded.logger.Level;
import virta.jet.embedded.server.Server;

public class Start {
	
	/**
	 * @param args
	 */
	public static void main(String[] args) {
		
		//creating server
		Server server = new Server(args);
		
		//Rewrite defaults (optional)		
		server.app().setLogLevel(Level.DEBUG);

		//add Handler
		server.setHandler("/","virta.jet.embedded.debug");
		//addServlet
		server.addServlet("RootServlet","/");
		server.addServlet("DebugServlet","/debug");
		server.addServlet("LangServlet","/lang");
		//start Server
		server.start();
	}

}
