package virta.jet.embedded.util;

import virta.jet.embedded.logger.AppLogger;
import virta.jet.embedded.logger.Level;
import virta.jet.embedded.struct.ConfStruct;

public class App {

	private static App self;
	
	
	//object fileds
	
	private App(){}
	
	private String accessLog;
	private String errorLog;
	private String pidFile;
	private String[] args;
	
	private ConfStruct config;
	
	private Throwable e;
	
	public static App getApp(){
		if(self==null){
			self = new App();
		}
		return self;
	}
	/**
	 * Reports current log level
	 *  
	 * @return log level
	 */
	public int getLogLevel(){
		return Level.AppLevel().getLevel();
	}
	/**
	 * Sets new log level
	 * 
	 * @param level
	 */
	public void setLogLevel(int level){
		Level.AppLevel().setLevel(level);
	}
	
	/**
	 * Returns logger
	 * @return logger
	 */
	public AppLogger getLogger(){
		return AppLogger.getLogger();
	}
	
	/**
	 * @return the accessLog
	 */
	public String getAccessLog() {
		return accessLog;
	}
	
	/**
	 * @param accessLog the accessLog to set
	 */
	public void setAccessLog(String accessLog) {
		this.accessLog = accessLog;
	}
	
	/**
	 * @return the errorLog
	 */
	public String getErrorLog() {
		return errorLog;
	}
	
	/**
	 * @param errorLog the errorLog to set
	 */
	public void setErrorLog(String errorLog) {
		this.errorLog = errorLog;
	}
	/**
	 * @return the pidFile
	 */
	public String getPidFile() {
		return pidFile;
	}
	/**
	 * @param pidFile the pidFile to set
	 */
	public void setPidFile(String pidFile) {
		this.pidFile = pidFile;
	}
	/**
	 * @return the args
	 */
	public String[] getArgs() {
		return args;
	}
	/**
	 * @param args the args to set
	 */
	public void setArgs(String[] args) {
		this.args = args;
	}
	/**
	 * @return the config
	 */
	public ConfStruct getConfig() {
		return config;
	}
	/**
	 * @param config the config to set
	 */
	public void setConfig(ConfStruct config) {
		this.config = config;
	}
	/**
	 * @return the e
	 */
	public boolean isAppClean() {
		return (e == null);
	}
	/**
	 * @param e the e to set
	 */
	public void setE(Throwable e) {
		if(e==null) return;
		
		this.e = e;
	}
}
