package virta.jet.embedded.util;

import java.io.File;
import java.io.IOException;

/**
 * Static library of methods
 * 
 * @author Alex Muravya (aka kyberorg) <asm@virtalab.net>
 *
 */
public final class Lib {
	/**
	 * Check if supplied file Resource is writable, if file don't exist it will be created, if dir is writable
	 * 
	 * @param fileName Name of Resource
	 * @return test result true (when file is writable or can be created
	 */
	public static boolean isValidFile(String fileName){
		File file = new File(fileName);
		if(file.isFile()){
			//file exists, check on write
			return file.canWrite();
		} else {
			//not exists, trying to create
			try {
				file.createNewFile();
			} catch (IOException e) {
				//cannot create
				return false;
			}
		}
		//if we reached this, that means we created file
		return true;
	}
}
