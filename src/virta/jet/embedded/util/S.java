package virta.jet.embedded.util;
/**
 * Static constants
 * 
 * @author Alex Muravya (aka kyberorg) <asm@virtalab.net>
 *
 */
public class S {
	public static final int MIN_PORT=1;
	public static final int ROOT_PORT=1024;
	public static final int MAX_PORT=65535;
}
