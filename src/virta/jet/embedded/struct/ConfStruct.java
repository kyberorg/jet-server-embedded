package virta.jet.embedded.struct;

import java.util.HashMap;

/**
 * Describes Structure of Config
 * 
 * @author Alex Muravya (aka kyberorg) <asm@virtalab.net>
 *
 */
public final class ConfStruct {
	private static ConfStruct self;
	
	private String context;
	private String mainPkg;
	private HashMap<String,String> servlets;
	
	/**
	 * Returns config
	 * 
	 * @param context path for current Handler
	 * @param mainPkg main package where I can find servlets
	 * @return ConfStruct  
	 */
	public static ConfStruct getConfig(String context, String mainPkg){
		if(self==null){
			self = new ConfStruct(context, mainPkg);
		}
		return self;
	}
	
	/**
	 * Constructor without servlets
	 * 
	 * @param context Context (Path from site root for this site). @see getContext 
	 * @param mainPkg main package, where Servlets are located
	 */
	private ConfStruct(String context,String mainPkg){
		this.context=context;
		this.mainPkg=mainPkg;
	}
	/**
	 * Returns Context (Path from site root for this site) 
	 * @return the context
	 */
	public String getContext() {
		return context;
	}
	/**
	 * Sets Context (Path from site root for this site) 
	 * Example: your Jet Site located at http://site/app (assume that http://site is served by nginx or other web server), 
	 * then context will "/app"
	 * @param context Context to set
	 */
	public void setContext(String context) {
		this.context = context;
	}
	
	
	/**
	 * Returns mainPkg
	 * @return the mainPkg
	 */
	public String getMainPkg() {
		return mainPkg;
	}
	
	/**
	 * Sets main package, where Servlets are located
	 * 
	 * @param mainPkg Main Package to set
	 */
	public void setMainPkg(String mainPkg) {
		this.mainPkg = mainPkg;
	}
	
	/**
	 * Returns Servets
	 * 
	 * @return the servlets
	 */
	public HashMap<String, String> getServlets() {
		return servlets;
	}
	/**
	 * Sets Servlets
	 * 
	 * @param servlets the servlets to set
	 */
	public void setServlets(HashMap<String, String> servlets) {
		this.servlets = servlets;
	}
	public void addServlet(String servletClass,String path){
		//if no HashMap, we set it
		if(servlets==null){
			HashMap<String, String> srvlts = new HashMap<String,String>();
			this.setServlets(srvlts);
		}
		//since now we work with servlets private prop
		this.servlets.put(servletClass, path);
	}
}
