package virta.jet.embedded.servlet;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import virta.jet.embedded.logger.WebLogger;
/**
 * Servlet with API 3.0 capability and logging enhainments
 * 
 * @author Alex Muravya (aka kyberorg) <asm@virtalab.net>
 *
 */
public class JetServlet extends HttpServlet {

	private static final long serialVersionUID = 4351481438512499982L;
	private static HttpServletRequest req;
	private static HttpServletResponse resp;
	
	/**
	 * Sets Request to private field 
	 * 
	 * @param req Unmodified HttpServletRequest
	 */
	protected static void sendRequest(HttpServletRequest req){
		JetServlet.req = req;
	}
	/**
	 * Sets Server Response as private field. Response should be unmodified after calling this method
	 * 
	 * @param resp Send-ready HttpServletResponse
	 */
	protected static void sendResponse(HttpServletResponse resp){
		JetServlet.resp = resp;
	}
	/**
	 * Adds to responce header with UTF-8 and text/html MIME-type
	 * 
	 * @throws ServletException when got not-valid Response or don't get get it at all
	 */
	protected static void useDefaults() throws ServletException{
		if(resp!=null){
			resp.setHeader("Content-Type", "text/html; charset=UTF-8");
		} else {
			throw new ServletException("Response is not set");
		}
	} 
	/**
	 * Writes response to Client-side (Browser or other reciever) 
	 * 
	 * @param body Text to Send 
	 * @throws IOException when cannot write to Client-side 
	 * @throws ServletException when got not-valid Response or don't get get it at all
	 */
	protected static void reply(String body) throws IOException, ServletException{
		if(resp!=null){
		//reply
		resp.getWriter().println(body);
		
		//logging
		if(req!=null){
		//and log (in future here will be a lot of logic, so just use log.web(req,resp);
		WebLogger logger = WebLogger.getLogger();
		logger.log(req, resp);
		
		}
	} else {
		throw new ServletException("Response is not set");
	}
	}
}
