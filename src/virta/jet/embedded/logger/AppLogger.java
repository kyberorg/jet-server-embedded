package virta.jet.embedded.logger;

import virta.sys.Shell;

public class AppLogger {
	//TODO timestamp
	
	private int level;
	
	private AppLogger(){
		
		this.level = Level.AppLevel().getLevel();
	}
	
	
	public static AppLogger getLogger(){
		return new AppLogger();
	}
	//TODO add handlers instead of console. And make parent with common method for App and Web Log + inface 
	
	public void debug(String str){
		if(level==Level.DEBUG || level == Level.ALL){
			
			//trace
			String fullClassName = Thread.currentThread().getStackTrace()[2].getClassName();            
            String className = fullClassName.substring(fullClassName.lastIndexOf(".") + 1);
            String methodName = Thread.currentThread().getStackTrace()[2].getMethodName();
            int lineNumber = Thread.currentThread().getStackTrace()[2].getLineNumber();
			
            String line = "DEBUG Message: "+str+" ["+className+".java: "+methodName+"() line " +lineNumber + "]";
            
            //TODO to handlers
            Shell.eecho(line);
		}
		
	}
	public void debug(Throwable ex){
		if(level==Level.DEBUG || level == Level.ALL){
			Shell.eecho("DEBUG Got Exception "+ ex.toString());
			Shell.eecho("Human readable message is: "+ ex.getMessage());
			Shell.eecho("Stack Trace is:");
			ex.printStackTrace(System.err);
		}
	}
	
	public void trace(String str){
		if(level==Level.TRACE || level == Level.ALL){
			
			String fullClassName = Thread.currentThread().getStackTrace()[2].getClassName();        
			String className = fullClassName.substring(fullClassName.lastIndexOf(".") + 1);
			int lineNumber = Thread.currentThread().getStackTrace()[2].getLineNumber();
			
			
			Shell.echo("TRACE: "+str + " ["+ className +":" + lineNumber+ "]");
		}
	}
	
	public void info(String str){
		if(level>=Level.INFO  || level == Level.ALL){
			Shell.echo("INFO "+str);
		}
	}
	
	public void warn(String str){
		if(level>=Level.WARNING  || level == Level.ALL){
			Shell.eecho("WARNING "+ str);
		}
	}
	
	public void warn(String mess, Throwable ex){
		if(level>=Level.WARNING  || level == Level.ALL){
			Shell.eecho("WARNING "+mess+": "+ex.getClass().getName());
		}
	}
	
	public void error(String str){
		if(level>=Level.ERROR  || level == Level.ALL){
			Shell.eecho("ERROR "+ str);
		}
	}
	
	public void error(String mess, Throwable ex){
		if(level>=Level.ERROR  || level == Level.ALL){
			Shell.eecho("ERROR "+mess+": "+ex.getClass().getName());
		}
	}
	
}
