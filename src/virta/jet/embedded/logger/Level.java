package virta.jet.embedded.logger;

public class Level {
	
	//predefined levels
	public static final int NONE = 0;
	public static final int ERROR = 5;
	public static final int WARNING = 7;
	public static final int INFO = 10;
	public static final int TRACE = 70;
	public static final int DEBUG = 100;
	public static final int ALL = 100000;
	
	private static Level self;
	
	private int level;
	
	private Level(){
		//default level
		//this.level = Level.INFO;
	}
	
	public static Level AppLevel(){
		if(self==null){
			self = new Level(); 
		}
		return self;
	} 
	
	public void setLevel(int level){
		this.level = level;
	}
	public int getLevel(){
		return level;
	}
	
	public int toInt(){
		return this.level;
	}
	
	
}
