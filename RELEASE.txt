TERMINATOR: This repository is terminated since now.
Futher development will be at Fork named WebJet.
See repo: https://bitbucket.org/virtaback/webjet
Hyvasti.  

Version-0.4.7:
A New RootServlet (with two links)
A Server reports PID to StdOut if pidFile not defined
M Few Internal changes

Version-0.4.5:
M Servlet API
A New LangServlet (encoding issue still unfixed)
M Modified DebugServlet 


Version-0.4: New Server API
M New Ant build scripts
M Server API
A Work with PID file. Application can write its PID to  file which set in -Dpid.file
M If file (access.log,error.log,pid.file) not exists, app will try to create it. If this won't be possible, warning will de displayed

Version-0.3: Server with logging

*New Ant build scripts
*Simple Web Logging implemented: to files and to console
*App Logging implemented

Version-0.2: Stable Server and Servlet with API

* Server launcher 
* ConfStruct: uses HashMap instread of Properties 
* Test build: build file, starter and servlet

Version 0.1: Test Release

At this state Server began works and nothing other features where implemented